package com.zdrsoft.jobprocessing.domain.task;

import java.util.LinkedList;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TasksRs {

    @JsonProperty("tasks")
    public LinkedList<Task> tasks = null;
}