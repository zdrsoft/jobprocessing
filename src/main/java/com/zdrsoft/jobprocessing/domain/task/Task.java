package com.zdrsoft.jobprocessing.domain.task;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Task {

    @JsonProperty("name")
    public String name;

    @JsonProperty("command")
    public String command;
}
