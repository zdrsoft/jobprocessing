package com.zdrsoft.jobprocessing.domain.task;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TasksRqItem extends Task {

    @JsonProperty("requires")
    public List<String> requires;
}
