package com.zdrsoft.jobprocessing.web.controller;

import com.zdrsoft.jobprocessing.domain.task.Task;
import com.zdrsoft.jobprocessing.domain.task.TasksRq;
import com.zdrsoft.jobprocessing.domain.task.TasksRs;
import com.zdrsoft.jobprocessing.service.TasksService;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.LinkedList;

@Slf4j
@Controller
@Getter
public class TasksController {

    private static final Logger logger = LoggerFactory.getLogger(TasksController.class);

    @Autowired
    TasksService tasksService;

    public static final String TASK_REORDER = "/";
    public static final String BASH_SCRIPT_PRODUCER = "/...";

    @RequestMapping(value = TASK_REORDER,
            method = {RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = { MediaType.APPLICATION_JSON_VALUE }
    )
    public @ResponseBody
    LinkedList<Task> tasksReorder(@RequestBody TasksRq tasksRq) {

        TasksRs tasksRs = getTasksService().prepareTasksRs(tasksRq);

        return tasksRs.getTasks();
    }

    @RequestMapping(value = BASH_SCRIPT_PRODUCER,
            method = {RequestMethod.POST},
            produces = MediaType.TEXT_PLAIN_VALUE,
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE
    )
    public @ResponseBody String tasksBashScriptExecutorProducer(@RequestBody String body) {

        try {
            String bashScript = getTasksService().prepareBashScript(body);

            return bashScript;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
