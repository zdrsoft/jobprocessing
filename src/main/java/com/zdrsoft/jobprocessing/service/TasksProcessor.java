package com.zdrsoft.jobprocessing.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zdrsoft.jobprocessing.domain.task.TasksRq;
import com.zdrsoft.jobprocessing.domain.task.TasksRqItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class TasksProcessor {

    private static final Logger logger = LoggerFactory.getLogger(TasksService.class);

    public LinkedList<TasksRqItem> processTaskReorder(TasksRq tasksRq) {

        LinkedList<TasksRqItem> tasksList = tasksRq.getTasks();
        TasksRqItem firstTasksRqItem = findTasksRqItemByRequires(tasksList, null);
        findAndReorderTasksRqItemByRequires(tasksList, firstTasksRqItem);

        for (int i = 0; i < tasksRq.getTasks().size() - 1; i++) {
            findAndReorderTasksRqItemByRequires(tasksList, tasksList.get(i));
        }

        return tasksList;
    }

    protected void findAndReorderTasksRqItemByRequires(LinkedList<TasksRqItem> reorderedTasksList, TasksRqItem nextTask) {

        String taskName = nextTask.getName();
        int nextTaskIndex = findTaskIndexByName(reorderedTasksList, taskName) + 1;
        int requiredTaskIndex = findTaskIndexByRequire(reorderedTasksList, taskName);

        /* Decrease by 1 nextTaskIndex value, if it is not available index (out of indexes scope) */
        if (nextTaskIndex == reorderedTasksList.size()) {
            nextTaskIndex-=1;
        }

        logger.debug("nextTaskIndex {}", nextTaskIndex);
        logger.debug("requiredTaskPosition {}", requiredTaskIndex);

        Collections.swap(reorderedTasksList, nextTaskIndex, requiredTaskIndex);
    }

    protected TasksRqItem findTasksRqItemsByName(LinkedList<TasksRqItem> tasksList, String taskName) {

        AtomicReference<TasksRqItem> tasksRqItemsRes = new AtomicReference();
        for (final TasksRqItem taskItem : tasksList) {
            if(taskItem.getName().equals(taskName)) {
                tasksRqItemsRes.set(taskItem);
                break;
            }
        }

        return tasksRqItemsRes.get();
    }

    protected int findTaskIndexByRequire(LinkedList<TasksRqItem> tasksList, String require) {
        int i = 0;
        int index = 0;
        int tasksListSize = tasksList.size();
        for(; i < tasksListSize; i++) {
            if (tasksList.get(i).getRequires() != null && tasksList.get(i).getRequires().indexOf(require) != -1) {
                index = i;
                break;
            }
        }

        return index;
    }

    protected int findTaskIndexByName(LinkedList<TasksRqItem> tasksList, String name) {
        int i = 0;
        int position = 0;
        int tasksListSize = tasksList.size();
        for(; i < tasksListSize; i++) {
            if (tasksList.get(i).getName().equals(name)) {
                position = i;
            }
        }

        return position;
    }

    protected TasksRqItem findTasksRqItemByRequires(List<TasksRqItem> tasksList, List<String> requires) {

        TasksRqItem taskItem = null;
        for (int i = 0; i < tasksList.size(); i++) {
            taskItem = tasksList.get(i);
            List<String> taskRequires = taskItem.getRequires();
            if (Objects.equals(taskRequires, requires)) break;
        }

        return taskItem;
    }

    protected <T> T convertJsonToObject(String json, Class<T> clazz) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }
}
