package com.zdrsoft.jobprocessing.service;

import com.zdrsoft.jobprocessing.domain.task.*;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;

@Getter
@Service
public class TasksService {

    private static final Logger logger = LoggerFactory.getLogger(TasksService.class);

    private static final String BASH_SCRIPT_FIRST_LINE = "#!/usr/bin/env bash";

    @Autowired
    private TasksProcessor tasksProcessor;

    public TasksRs prepareTasksRs(TasksRq tasksRq) {

        LinkedList<TasksRqItem> tasksList = getTasksProcessor().processTaskReorder(tasksRq);

        LinkedList<Task> tasks = new LinkedList<>();
        for (TasksRqItem task : tasksList) {
            task.setRequires(null);
            tasks.add(task);
        }

        TasksRs tasksRs = new TasksRs();
        tasksRs.setTasks(tasks);

        return tasksRs;
    }

    public String prepareBashScript(String body) throws IOException {

        String decoded = URLDecoder.decode(body, StandardCharsets.UTF_8.name());

        TasksRq tasksRq = getTasksProcessor().convertJsonToObject(decoded, TasksRq.class);

        logger.debug("decoded {}", decoded);

        LinkedList<TasksRqItem> tasksList = getTasksProcessor().processTaskReorder(tasksRq);

        StringBuilder bashScript = new StringBuilder();
        bashScript.append(BASH_SCRIPT_FIRST_LINE).append("\n\n");

        for (TasksRqItem task : tasksList) {
            bashScript.append(task.getCommand()).append("\n");
        }

        return bashScript.toString();
    }

}
