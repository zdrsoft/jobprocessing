package com.zdrsoft.jobprocessing.service;

import com.zdrsoft.jobprocessing.AbstractTest;
import com.zdrsoft.jobprocessing.domain.task.TasksRq;
import com.zdrsoft.jobprocessing.domain.task.TasksRqItem;
import lombok.Getter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Getter
@SpringBootTest
public class TaskProcessorTest extends AbstractTest {

    @Autowired
    TasksProcessor tasksProcessor;

    @Test
    public void findTasksRqItemsByNameTest() throws IOException {
        TasksRq tasksRq = getTasksRqFromData();

        TasksRqItem expectedTaskItem = new TasksRqItem();
        expectedTaskItem.setName("task-1");
        expectedTaskItem.setCommand("touch /tmp/file1");

        LinkedList<TasksRqItem> tasks = (LinkedList<TasksRqItem>) tasksRq.getTasks();

        TasksRqItem tasksRqItem = getTasksProcessor().findTasksRqItemsByName(tasks, "task-1");
        assertThat(tasksRqItem).isEqualToComparingFieldByField(expectedTaskItem);

        TasksRqItem tasksRqItem1 = getTasksProcessor().findTasksRqItemsByName(tasks, "task-11");
        assertThat(tasksRqItem1).isEqualTo(null);
    }

    @Test
    public void findTasksRqItemByRequiresTest() throws IOException {
        TasksRq tasksRq = getTasksRqFromData();
        List<TasksRqItem> tasks = tasksRq.getTasks();

        List<String> requires = new ArrayList<>();
        requires.add("task-2");
        requires.add("task-3");

        TasksRqItem tasksRqItem = getTasksProcessor().findTasksRqItemByRequires(tasks, requires);
        assertThat(tasksRqItem.getRequires()).isEqualTo(requires);
        assertThat(tasksRqItem.getName()).isEqualTo("task-4");

        TasksRqItem tasksRqItem1 = getTasksProcessor().findTasksRqItemByRequires(tasks, null);
        assertThat(tasksRqItem1.getRequires()).isEqualTo(null);
        assertThat(tasksRqItem1.getName()).isEqualTo("task-1");
    }

    @Test
    public void findTaskIndexByNameTest() throws IOException {
        TasksRq tasksRq = getTasksRqFromData();
        LinkedList<TasksRqItem> tasks = tasksRq.getTasks();

        int index = getTasksProcessor().findTaskIndexByName(tasks, "task-4");
        logger.info("Found index: {}", index);
        assertThat(index).isEqualTo(3);

        index = getTasksProcessor().findTaskIndexByName(tasks, "task-1");
        logger.info("Found index: {}", index);
        assertThat(index).isEqualTo(0);
    }

    @Test
    public void findTaskIndexByRequireTest() throws IOException {
        TasksRq tasksRq = getTasksRqFromData();
        LinkedList<TasksRqItem> tasks = tasksRq.getTasks();

        int index = getTasksProcessor().findTaskIndexByRequire(tasks, null);
        logger.info("Found index: {}", index);
        assertThat(index).isEqualTo(0);

        index = getTasksProcessor().findTaskIndexByRequire(tasks, "task-1");
        logger.info("Found index: {}", index);
        assertThat(index).isEqualTo(2);
    }
}
