package com.zdrsoft.jobprocessing.service;

import com.zdrsoft.jobprocessing.AbstractTest;
import com.zdrsoft.jobprocessing.domain.task.TasksRq;
import com.zdrsoft.jobprocessing.domain.task.TasksRqItem;
import lombok.Getter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Getter
@SpringBootTest
public class TasksServiceTest extends AbstractTest {

    @Autowired
    TasksService tasksService;

    @Test
    public void tasksServiceTest() {
        assertThat(tasksService).isNotNull();
    }

}
