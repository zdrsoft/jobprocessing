package com.zdrsoft.jobprocessing;

import com.zdrsoft.jobprocessing.web.controller.TasksController;
import lombok.Getter;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

@Getter
@SpringBootTest
public class TasksControllerTests extends AbstractTest {

    @Autowired
    private TasksController controller;

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }

    @Test
    public void tasksReorderTasksControllerTest() {
        assertThat(controller).isNotNull();
    }

    @Test
    public void tasksReorderTasksControllerStatusTest() throws Exception {

        Resource jsonProcessingTaskRqRes = getJobProcessingTasksRq();
        String inputJson = IOUtils.toString(jsonProcessingTaskRqRes.getInputStream());

        logger.info("Input JobProcessingTasks json:\n{}", inputJson);

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(TasksController.TASK_REORDER)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(inputJson)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        String expectedReorderedTasks = "[{\"name\":\"task-1\",\"command\":\"touch /tmp/file1\"},{\"name\":\"task-3\",\"command\":\"echo 'Hello World!' > /tmp/file1\"},{\"name\":\"task-2\",\"command\":\"cat /tmp/file1\"},{\"name\":\"task-4\",\"command\":\"rm /tmp/file1\"}]";
        logger.info("Output JobProcessingTasks json:\n{}", content);
        assertEquals(content, expectedReorderedTasks);
    }

}
