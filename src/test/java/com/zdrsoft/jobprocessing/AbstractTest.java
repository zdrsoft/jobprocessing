package com.zdrsoft.jobprocessing;

import java.io.IOException;

import com.zdrsoft.jobprocessing.domain.task.TasksRq;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = JobProcessingApplication.class)
@WebAppConfiguration
@Getter
@Setter
public abstract class AbstractTest {

    protected static final Logger logger = LoggerFactory.getLogger(AbstractTest.class);

    protected MockMvc mvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Value("classpath:/data/jobsRequest.json")
    public Resource jobProcessingTasksRq;

    protected void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    protected String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }
    protected <T> T mapFromJson(String json, Class<T> clazz) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, clazz);
    }

    public TasksRq getTasksRqFromData() throws IOException {
        Resource jsonProcessingTaskRqRes = getJobProcessingTasksRq();
        String inputJson = IOUtils.toString(jsonProcessingTaskRqRes.getInputStream());

        return mapFromJson(inputJson, TasksRq.class);
    }
}